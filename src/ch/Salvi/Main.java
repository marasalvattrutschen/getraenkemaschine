
//**************************************************************************
//
// Hauptklasse
//
// Erstellt von: Salvatore Aronica
// Erstellungsdatum: 01.03.2018
// Geändert von: Mara Issa-Khani
// Änderungsdatum: 16.03.2018
//
//
//**************************************************************************

package ch.Salvi;

import java.util.ArrayList;
import java.util.List;

// Main Funktion --> Steuert das gesamte Programm
// Erstellt von: Salvatore Aronica
// Erstellt am: 01.03.2018
// Geändert von: Mara Issa-Khani
// Geändert am: 16.03.2018
// Quelle: Literatur "Java ist auch eine Insel"
public class Main
{

    // Getränk Array initialisieren
    public static List<Drinks> DrinkArray = new ArrayList<Drinks>();

    // Globale Variable BETRAG
    protected static double glob_int_Money;

    // Hauptfunktion
    public static void main(String[] args)
    {

        // CSV lesen und Daten in Array schreiben
        Functions.EinlesenCSV();

        // Frame von Klasse aufrufen
        FrameMain MainFrame = new FrameMain();
        MainFrame.setContentPane(new FrameMain().PanelMain);


        // Auf Visible setzen
        MainFrame.pack();
        MainFrame.setVisible(true);

    }
}


