
//**************************************************************************
//
// Function-Klasse
// Alle Nebenfunktionen werden hier aufgelistet
//
// Erstellt von: Salvatore Aronica
// Erstellungsdatum: 05.03.2018
// Geändert von: Salvatore Aronica
// Änderungsdatum: 14.03.2018
//
//**************************************************************************

package ch.Salvi;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Scanner;

import static ch.Salvi.Main.DrinkArray;

public class Functions {

    // Funktion zum auslesen des CSV
    // Speichert die Getränke in ein globales ARRAY
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 05.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 13.03.2018
    // Quelle : https://www.youtube.com/watch?v=QeaXXpxNtq0
    static void EinlesenCSV() {
        // Initialisieren
        Scanner CSVScan = null;
        int int_Rowc = 0;
        String InputLine = "";
        String xfileLocation;


        // Pfad angabe zu CSV
        xfileLocation = "Getraenke.csv";

        try {
                // Scanner intitialisieren
                CSVScan = new Scanner(new BufferedReader(new FileReader(xfileLocation)));

                // CSV header überspringen
                InputLine = CSVScan.nextLine();

                // CSV durchloopen
                while (CSVScan.hasNextLine()) {
                    // Linie lesen
                    InputLine = CSVScan.nextLine();

                    // Zeile splitten und Temp-Array schreiben
                    String[] InArray = InputLine.split(";");

                    //Klasse Getränk erstellen
                    DrinkArray.add(new Drinks(Integer.parseInt(InArray[0]), String.valueOf(InArray[1]), Double.parseDouble(InArray[2]), new ImageIcon("picture\\" + String.valueOf(InArray[0]) + ".png")));

                    int_Rowc++;

                }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // W2
    // Funktion zum skalieren von Bildern
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 07.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 07.03.2018
    //Quelle: https://stackoverflow.com/questions/244164/how-can-i-resize-an-image-using-java
    public static BufferedImage resizeImage (BufferedImage image, int areaWidth, int areaHeight) {
        //Deklaration und Initialisierung
        float scaleX = (float) areaWidth / image.getWidth();
        float scaleY = (float) areaHeight / image.getHeight();
        float scale = Math.min(scaleX, scaleY);
        int w = Math.round(image.getWidth() * scale);
        int h = Math.round(image.getHeight() * scale);
        int type = image.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        boolean scaleDown = scale < 1;

        //Prüfen ob skaliert nach oben oder nach unten durchgeführt werden muss
        if (scaleDown) {
            int currentW = image.getWidth();
            int currentH = image.getHeight();
            BufferedImage resized = image;

            //Wenn grösser oder kleiner Skalierung
            while (currentW > w || currentH > h) {
                currentW = Math.max(w, currentW / 2);
                currentH = Math.max(h, currentH / 2);

                // Skalierung vorbereiten
                BufferedImage temp = new BufferedImage(currentW, currentH, type);

                // Icon färben
                Graphics2D g2 = temp.createGraphics();

                // Skalieren
                g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);

                // Icon färben
                g2.drawImage(resized, 0, 0, currentW, currentH, null);
                g2.dispose();

                // Zurückgeben
                resized = temp;
            }
            return resized;
        } else {
            Object hint = scale > 2 ? RenderingHints.VALUE_INTERPOLATION_BICUBIC : RenderingHints.VALUE_INTERPOLATION_BILINEAR;

            // Skalierung vorbereiten
            BufferedImage resized = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

            // Icon färben
            Graphics2D g2 = resized.createGraphics();

            // Skalieren
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);

            // Icon färben
            g2.drawImage(image, 0, 0, w, h, null);
            g2.dispose();

            // Zurückgeben
            return resized;
        }
    }

    // W3
    // Fügt Bild ein
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 13.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 13.03.2018
    // Quelle: https://stackoverflow.com/questions/17383867/set-icon-image-in-java
    static void SetPicture(String _path, JLabel _label, JTextField _textfield1, JTextField  _textfield2, double _price, String _text) {

        //Decimalformat damit .00 angezeigt wird
        DecimalFormat f = new DecimalFormat("#0.00");

        // Initialisieren
        Icon icon = null;

        // W2: Bild laden
        icon = new ImageIcon(_path);

        // W2: Skalierung vorbereiten
        BufferedImage bi1 = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);

        // W2: Icon färben
        Graphics g1 = bi1.createGraphics();
        icon.paintIcon(null, g1, 0, 0);

        // W2: Für Resize übergeben --> FUNCTIONS.RESIZEIMAGE
        // W2: In ImageIcon laden --> New ImageIcon
        // W2: In Label laden --> set Icon
        _label.setIcon(new ImageIcon(Functions.resizeImage(bi1, 100, 100)));
        _textfield1.setText(f.format(_price));
        _textfield2.setText(_text);

    }

    // W3
    // Fügt Bild ein -- Überladen
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 13.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 13.03.2018
    static void SetPicture(String _path, JLabel _label) {

        // Initialisieren
        Icon icon = null;

        // W2: Bild laden
        icon = new ImageIcon(_path);

        // W2: Skalierung vorbereiten
        BufferedImage bi1 = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);

        // W2: Icon färben
        Graphics g1 = bi1.createGraphics();
        icon.paintIcon(null, g1, 0, 0);

        // W2: Für Resize übergeben --> FUNCTIONS.RESIZEIMAGE
        // W2: In ImageIcon laden --> New ImageIcon
        // W2: In Label laden --> set Icon
        _label.setIcon(new ImageIcon(Functions.resizeImage(bi1, 100, 100)));

    }

}
