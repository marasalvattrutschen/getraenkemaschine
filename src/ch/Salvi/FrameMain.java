
//**************************************************************************
//
// Hauptform zum Getränke rauslassen
// Dazugehörige FORM: FrameMain.form
//
//
// Erstellt von: Salvatore Aronica
// Erstellungsdatum: 05.03.2018
// Geändert von: Mara Issa-Khani
// Änderungsdatum: 16.03.2018
//
//**************************************************************************

package ch.Salvi;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;

import static ch.Salvi.Main.DrinkArray;
import static ch.Salvi.Main.glob_int_Money;

public class FrameMain extends JFrame{
    protected JPanel PanelMain, PanelLeft, PanelTop, PanelRight, PanelOutput;
    private JButton ButtonMainClose, ButtonMainDeposit, ButtonMainBuy, ButtonMainBack;
    protected JTextField TextFieldLeftCosts0, TextFieldLeftCosts1, TextFieldLeftCosts2, TextFieldLeftCosts3, TextFieldLeftCosts4, TextFieldLeftCosts5, TextFieldLeftCosts6, TextFieldLeftCosts7, TextFieldLeftCosts8;
    private JTextField TextFieldTitel;
    protected JFormattedTextField TextFieldRightMoney;
    private JLabel LabelLeftOutput, LabelLeft0, LabelLeft1, LabelLeft2, LabelLeft3, LabelLeft4, LabelLeft5, LabelLeft6, LabelLeft7, LabelLeft8;
    private static JLabel[] LeftLabels = new JLabel[9];
    private static JTextField[] LeftTextFieldCosts = new JTextField[9];
    private static JTextField[] LeftTextFieldName = new JTextField[9];
    private JTextPane TextFieldRightInfo;
    private JTextField TextFieldLeftOutputCost, TextFieldLeftOutputName;
    private JTextField TextFieldLeftName0, TextFieldLeftName1, TextFieldLeftName2, TextFieldLeftName3, TextFieldLeftName4, TextFieldLeftName5, TextFieldLeftName6, TextFieldLeftName7, TextFieldLeftName8;
    private JButton ButtonMainCancelChoice;
    Icon icon1 = null;
    String pathpicture = "picture//";


    // Konstruktor von FrameMain
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 05.03.2018
    // Geändert von: Mara Issa-Khani
    // Geändert am: 16.03.2018
    // Quelle: Literatur "Java ist auch eine Insel"
    public FrameMain() {
        int int_counter = 0;

        //Decimalformat damit .00 angezeigt wird
        DecimalFormat f = new DecimalFormat("#0.00");

        // Close Aktion hinterlegen
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Titel setzen
        this.setTitle("Getränke-Automat");

        // W2 Jlabels in Array setzen
        JLabel[] LeftLabels = {LabelLeft0, LabelLeft1, LabelLeft2, LabelLeft3, LabelLeft4, LabelLeft5, LabelLeft6, LabelLeft7, LabelLeft8};

        // W2 JTextFieldCosts in Array setzen
        JTextField[] LeftTextFieldCosts = {TextFieldLeftCosts0, TextFieldLeftCosts1, TextFieldLeftCosts2, TextFieldLeftCosts3, TextFieldLeftCosts4, TextFieldLeftCosts5, TextFieldLeftCosts6, TextFieldLeftCosts7, TextFieldLeftCosts8};

        // W2 JTextFieldName in Array setzen
        JTextField[] LeftTextFieldName = {TextFieldLeftName0, TextFieldLeftName1, TextFieldLeftName2, TextFieldLeftName3, TextFieldLeftName4, TextFieldLeftName5, TextFieldLeftName6, TextFieldLeftName7, TextFieldLeftName8};

        // W2 TextPane Textfluss in die Mitte setzen
        StyledDocument doc = TextFieldRightInfo.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);

        // W2 List Array loopen damit Preise und Bilder hinterlegt werden können.
        for(int x= 0; x < DrinkArray.size(); x++)
        {
            // W2: Bild Text und Preis laden
            Functions.SetPicture(pathpicture + DrinkArray.get(x).getID() + ".jpg", LeftLabels[x], LeftTextFieldCosts[x], LeftTextFieldName[x], DrinkArray.get(x).getPrice(), DrinkArray.get(x).getName());
        }

        // Format von TextFeld Betrag ändern
        TextFieldRightMoney.setValue(f.format(0.00));

        // Aktion Klick auf Button schliessen
        ButtonMainClose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dispose();
                System.exit(0);
            }
        });

        // W2: Aktion Klick auf Button Geld hinterlegen
        ButtonMainDeposit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Funktion aufrufen
                showFramedeposit();
            }
        });

        // Aktion Klick auf Button Geld zurück
        ButtonMainBack.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Feld auf Null setzen
                setTextFieldRightMoney(0.00);
                // Textinfo
                setTextFieldRightInfo(String.valueOf(f.format(glob_int_Money)) + "\nwurden ausgeworfen.");
                // Globale Variable auf Null setzen
                glob_int_Money = 0.00;

            }
        });

        // W2: Aktion Klick auf Button kaufen
        ButtonMainBuy.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                // W2: Prüfen ob Auswahl getroffen wurde
                if (!TextFieldLeftOutputCost.getText().equals("")) {
                    // W2: Prüfen ob genug Geld vorhanden ist
                    if (glob_int_Money > Double.parseDouble(TextFieldLeftOutputCost.getText())) {
                        // W2: Betrag Minusrechnen
                        glob_int_Money = glob_int_Money - Double.parseDouble(TextFieldLeftOutputCost.getText());

                        // W2 Info ausgeben
                        setTextFieldRightInfo(TextFieldLeftOutputName.getText() + " wurde für\n" + String.valueOf(f.format(Double.parseDouble(TextFieldLeftOutputCost.getText()))) + " gekauft.\nRückgabe Geld von\n" + f.format(glob_int_Money) + " ausgegeben.");

                        // Feld auf Null setzen
                        setTextFieldRightMoney(0.00);

                        // Globale Variable auf Null setzen
                        glob_int_Money = 0.00;

                        // W2: Output nullen
                        CancelOutput();
                    }
                    else {
                        setTextFieldRightInfo("Betrag ist zu klein.\nZuerst Geld einwerfen");

                    }
                }
                else {
                    setTextFieldRightInfo("Sie haben keine\n Auswahl getroffen.\nBitte wählen Sie\nein Getränk");
                }
            }
        });

        // W2: Aktion Klick auf Getränk1
        LabelLeft0.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(0).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(0).getPrice(), DrinkArray.get(0).getName());
            }
        });

        // W2: Aktion Klick auf Getränk2
        LabelLeft1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(1).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(1).getPrice(), DrinkArray.get(1).getName());
            }
        });

        // W2: Aktion Klick auf Getränk3
        LabelLeft2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(2).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(2).getPrice(), DrinkArray.get(2).getName());
            }
        });

        // W2: Aktion Klick auf Getränk4
        LabelLeft3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(3).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(3).getPrice(), DrinkArray.get(3).getName());
            }
        });

        // W2: Aktion Klick auf Getränk5
        LabelLeft4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(4).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(4).getPrice(), DrinkArray.get(4).getName());
            }
        });

        // W2: Aktion Klick auf Getränk6
        LabelLeft5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(5).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(5).getPrice(), DrinkArray.get(5).getName());
            }
        });

        // W2: Aktion Klick auf Getränk7
        LabelLeft6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(6).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(6).getPrice(), DrinkArray.get(6).getName());
            }
        });

        // W2: Aktion Klick auf Getränk8
        LabelLeft7.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(7).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(7).getPrice(), DrinkArray.get(7).getName());
            }
        });

        // W2: Aktion Klick auf Getränk9
        LabelLeft8.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Bild Text und Preis laden
                Functions.SetPicture(pathpicture + DrinkArray.get(8).getID() + ".jpg", LabelLeftOutput, TextFieldLeftOutputCost, TextFieldLeftOutputName, DrinkArray.get(8).getPrice(), DrinkArray.get(8).getName());
            }
        });

        // W2: Aktion Klick auf Auswahl löschen
        ButtonMainCancelChoice.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // W2: Output nullen
                CancelOutput();
            }
        });
    }

    // Get-Set-Methoden
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 06.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 06.03.2018
    public void setTextFieldRightMoney(double _Betrag)
    {
        // Decimalformat damit .00 angezeigt wird
        DecimalFormat f = new DecimalFormat("#0.00");
        // Wert setzen
        this.TextFieldRightMoney.setValue(f.format(_Betrag));
    }

    public void setTextFieldRightInfo(String _Info)
    {
        // Wert setzen
        this.TextFieldRightInfo.setText(_Info);
    }

    // ausgegliederte Funktionen
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 06.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 06.03.2018

    // Ruft Fenster zum Geld aufladen auf
    public void showFramedeposit(){
        FrameMoneyDeposit FrameDeposit = new FrameMoneyDeposit(this);
    }

    // W3: Setzt Output Felder auf "NULL"
    public void CancelOutput(){
        // W2: Output nullen
        LabelLeftOutput.setIcon(null);
        LabelLeftOutput.revalidate();
        TextFieldLeftOutputCost.setText("");
        TextFieldLeftOutputName.setText("");
    }
}
