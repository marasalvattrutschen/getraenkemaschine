
//**************************************************************************
//
// Getränke Klasse
// Getränke werden Initialisiert
//
// Erstellt von: Salvatore Aronica
// Erstellungsdatum: 01.03.2018
// Geändert von: Mara Issa-Khani
// Änderungsdatum: 09.03.2018
//
//**************************************************************************

package ch.Salvi;

import javax.swing.*;
import java.text.DecimalFormat;

public class Drinks {

    // Globale Variablen Initialisieren
    protected int int_id;
    protected String str_name;
    protected double dou_price;
    protected ImageIcon img_picture;

    // Decimalformat damit .00 angezeigt wird
    DecimalFormat f = new DecimalFormat("#0.00");

    // Konstruktor von Klasse Drinks
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 01.03.2018
    // Geändert von: Mara Issa-Khani
    // Geändert am: 16.03.2018
    // Quelle: Literatur "Java ist auch eine Insel"
    public Drinks(int _id, String _name, double _price, ImageIcon _picture)
    {
        this.int_id = _id;
        this.str_name = _name;
        this.dou_price = _price;
        this.img_picture = _picture;
    }

    // Get-Set-Methoden
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 01.03.2018
    // Geändert von: Salvatore Aronica
    // Geändert am: 09.03.2018
    public int getID() { return int_id; }

    public void setID(int _id)
    {
        this.int_id = _id;
    }

    public String getName() { return str_name; }

    public void setName(String _name)
    {
        this.str_name = _name;
    }

    public double getPrice()
    {
        return dou_price;
    }

    public void setPrice(double _price)
    {
        this.dou_price = _price;
    }
}
