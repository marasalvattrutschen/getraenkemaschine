
//**************************************************************************
//
// Form Geld hinterlegen
// Dazugehörige FORM: FrameMOneyDeposit.form
// Betrag wird erhöht
//
// Erstellt von: Salvatore Aronica
// Erstellungsdatum: 05.03.2018
// Geändert von: Mara Issa-Khani
// Änderungsdatum: 16.03.2018
//
//**************************************************************************

package ch.Salvi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import static ch.Salvi.Main.glob_int_Money;

public class FrameMoneyDeposit{

    //Globale Variablen
    protected JFrame FrameDeposit;
    protected JFormattedTextField TextFiledDepo;
    private JButton ButtonDepoDeposit, ButtonDepoClose;
    private JPanel PanelDepoMain, PanelCoins, PanelBottom;
    private JLabel LabelDepo1, LabelDepo2, LabelDepo3, LabelDepo4, LabelDepo5, LabelDepo6;
    String pathpicture = "picture//";


    // Konstruktor von FrameMoneyDeposit
    // Erstellt von: Salvatore Aronica
    // Erstellt am: 05.03.2018
    // Geändert von: Mara Issa-Khani
    // Geändert am: 16.03.2018
    // Quelle: Literatur "Java ist auch eine Insel"
    public FrameMoneyDeposit(FrameMain frameMain) throws HeadlessException {

        // Klasse JFRAME instanzieren
        FrameDeposit = new JFrame();

        // Panel hinterlegen "PanelDepoMain"
        FrameDeposit.add(PanelDepoMain);

        //Decimalformat damit .00 angezeigt wird
        DecimalFormat f = new DecimalFormat("#0.00");

        // Format von TextFeld Auladen ändern
        TextFiledDepo.setValue(f.format(0.00));

        // Close Aktion hinterlegen
        FrameDeposit.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Titel setzen
        FrameDeposit.setTitle("Getränke-Automat");

        // W2: Bild laden
        Functions.SetPicture(pathpicture + "10rp.png", LabelDepo1);
        Functions.SetPicture(pathpicture + "20rp.png", LabelDepo2);
        Functions.SetPicture(pathpicture + "50rp.png", LabelDepo3);
        Functions.SetPicture(pathpicture + "1chf.png", LabelDepo4);
        Functions.SetPicture(pathpicture + "2chf.png", LabelDepo5);
        Functions.SetPicture(pathpicture + "5chf.png", LabelDepo6);

        // Passt das Fenster auf die Kleinstmögliche grösse an
        FrameDeposit.pack();

        // Auf Visible setzen
        FrameDeposit.setVisible(true);

        // Aktion Klick auf Button Schliessen
        ButtonDepoClose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FrameDeposit.dispose();
            }
        });

        // Aktion Klick auf Button Aufladen
        ButtonDepoDeposit.addMouseListener(new MouseAdapter() { public void mouseClicked(MouseEvent e) {
                glob_int_Money = glob_int_Money + Double.parseDouble(TextFiledDepo.getText());
                frameMain.setTextFieldRightMoney(glob_int_Money);
                frameMain.setTextFieldRightInfo(TextFiledDepo.getText() + " wurden aufgeladen. \n Wählen Sie ein Getränk.");
                FrameDeposit.dispose();
            }
        });

        // W2: Aktion Klick auf Image 10RP.
        LabelDepo1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 10rp. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+0.10));
            }
        });

        // W2: Aktion Klick auf Image 20RP.
        LabelDepo2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 20rp. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+0.20));
            }
        });

        // W2: Aktion Klick auf Image 50RP.
        LabelDepo3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 50rp. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+0.50));
            }
        });

        // W2: Aktion Klick auf Image 1CHF.
        LabelDepo4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 1CHF. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+1.00));
            }
        });

        // W2: Aktion Klick auf Image 2CHF.
        LabelDepo5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 2CHF. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+2.00));
            }
        });

        // W2: Aktion Klick auf Image 5CHF.
        LabelDepo6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Betrag um 5CHF. erhöhen
                TextFiledDepo.setValue(f.format(Double.parseDouble(TextFiledDepo.getText())+5.00));
            }
        });
    }

}

